# Muzik - Music Player
An awesome Web Music player projects!

## About The Project

[![Muzik App][product-screenshot]](https://mymuzik.herokuapp.com/)

This project is about Music player that created by using ReactJS and some others library like Material UI, and also Google OAuth. This is music player project have PWA features too if you want to try it, so it can be installed on your Desktop and Mobile apps. 

Here's the features that can be used by this Apps:
* OAuth Login with Google
* Play song
* Browse song
* Create playlist and also Show playlist page
* Loved Song and Loved song Page
* Profile page (Update Profile)



### Built With

Here the another Library that used to build this projects
* [React JS](https://reactjs.org/)
* [Material UI](https://material-ui.com/)
* [Styled Components](https://styled-components.com/)
* [Axios](https://axios-http.com/)
* [Redux Saga](https://redux-saga.js.org/)
* [React Google Login](https://anthonyjgrove.github.io/react-google-login)




## Getting Started

Here is the instructions on how you can configure to your own local repository.
To get a local copy up and running follow these simple example steps.

### Prerequisites

Before First step, install the `npm`
* npm
  ```sh
  npm install npm@latest -g
  ```

### Installation

1. Get a your own Google CLIENT ID Key at [Google OAuth](https://developers.google.com/identity/sign-in/web/sign-in) to create credentials with Google OAuth
2. Create `.env` file and Copy your Client ID
3. Clone the repo
   ```sh
   git clone https://bitbucket.org/aindrajayaa/muzik.git
   ```
5. Install NPM packages
   ```sh
   npm install
   ```
6. Enter your API in `.env`
   ```dosini
   REACT_APP_GOOGLE_CLIENT_ID = 'ENTER YOUR API CLIENT ID URL';
   ```



## Roadmap

See the [open issues](https://bitbucket.org/aindrajayaa/muzik/issues) for a list of proposed features (and known issues).



## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request



## License

Distributed under the MIT License. See `LICENSE` for more information.



## Contact

Your Name - [@your_twitter](https://twitter.com/Arista_Indra) - arista.indrajaya@gmail.com

Project Link: [https://bitbucket.org/aindrajayaa/muzik](https://bitbucket.org/aindrajayaa/muzik)



## Acknowledgements
* [Yup](https://www.webpagefx.com/tools/emoji-cheat-sheet)
* [Img Shields](https://shields.io)




[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/in/aindrajaya/
[product-screenshot]: src/assets/screenshot.png